# bashrc file for the images

# set up command line
PS1='\[\033[01;31m\][\h:\u]\[\033[01;32m\]:\W>\[\033[00m\] '
CLICOLOR=1

alias ls='ls --color=auto'

# enable tab complete
source /etc/profile.d/bash_completion.sh
source /home/atlas/release_setup.sh

# add lwtnn converters
PATH+=:/usr/local/converters

# get configuration settings
ln -sf /home/atlas/settings/git/config ~/.gitconfig
ln -sf /home/atlas/settings/ssh ~/.ssh

HISTFILESIZE=""
HISTSIZE=""
