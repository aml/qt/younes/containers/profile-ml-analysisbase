FROM atlas/analysisbase:21.2.136

USER root
RUN \
  yum update -y && \
  pushd /tmp/ && \
  wget https://repo.ius.io/ius-release-el7.rpm && \
  rpm -i --nodeps ius-release-el7.rpm && \
  popd && \
  yum remove -y git && \
  yum install -y git222 && \
  yum install -y jq tree bash-completion && \
  yum clean all && \
  true

# install basic utilities
RUN \
  yum install -y valgrind-devel && \
  true

# install additional things (if they become stable move to the above list)
RUN \
  true

COPY custom-rc.bash /home/atlas/.bashrc
COPY custom_logout.bash /home/atlas/.bash_logout

USER atlas
WORKDIR /home/atlas/work

CMD /bin/bash --rcfile ${HOME}/.bashrc
